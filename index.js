'use strict';

const express = require('express');

// Constants
const PORT = 8091;
const HOST = '192.99.145.229';

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello world\n');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
